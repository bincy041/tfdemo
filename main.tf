resource "azurerm_resource_group" "example" {
  name    =regex("^[-\\w\\._\\(\\)]+$",("rg-${local.example[0]}"))
  location = "East US"
}